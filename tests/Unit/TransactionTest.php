<?php

namespace Tests\Unit;

use Tests\TestCase;

class TransactionTest extends TestCase
{
    /**
     * unit test to get product list.
     *
     * @return void
     */
    public function testProductList()
    {
        $response = $this->json('GET', '/api/products');
        $response->assertStatus(200);
    }
    
    /**
     * unit test to get transactions.
     *
     * @return void
     */
    public function testtransactionList()
    {
        $response = $this->json('GET', '/api/transactions');
        $response->assertStatus(200);
    }
    
    /**
     * unit test to create transaction.
     *
     * @return void
     */
    public function testCreateTransaction()
    {
        $data = [
            'start_date'       => '20/12/2019',
            'end_date'         => '25/12/2019',
            'first_name'       => 'Fatima',
            'last_name'        => 'Umer', 
            'email'            => 'fu@yahoo.com',
            'telnumber'        => '9258325555',
            'address1'	       => 'abc',
            'address2'         => 'xyz',
            'city'             => 'Lahore',
            'country'          => 'Pakistan',
            'postcode'         => '54000',
            'product_name'     => 'DYSIH_DE_PLATINUM_25-30',
            'cost'             => '25.75',
            'currency'         => 'usd',
            'transaction_date' => '20/12/2019'
        ];
        
        $response = $this->json('POST', '/api/transaction/create', $data);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'created',
            ]);
    }
    
    /**
     * unit test to update transaction.
     *
     * @return void
     */    
    public function testUpdateTransaction()
    {
        $transactionId = $this->getTransactionId();
        
        $data = [
            'start_date'       => '20/12/2019',
            'end_date'         => '25/12/2019',
            'first_name'       => 'Fatima',
            'last_name'        => 'Umer', 
            'email'            => 'fu@yahoo.com',
            'telnumber'        => '9258325555',
            'address1'	       => 'address is updated',
            'address2'         => 'xyz',
            'city'             => 'Lahore',
            'country'          => 'Pakistan',
            'postcode'         => '54000',
            'product_name'     => 'DYSIH_DE_PLATINUM_25-30',
            'cost'             => '25.75',
            'currency'         => 'usd',
            'transaction_date' => '20/12/2019'
        ];
        
        $update = $this->json('PATCH', '/api/transaction/'.$transactionId, $data);
        
        $update
            ->assertStatus(200)
            ->assertJson([
                'status' => 'updated',
            ]);
    }  
    
    /**
     * unit test to delete transaction.
     *
     * @return void
     */   
    public function testDeleteTransaction()
    {
        $transactionId = $this->getTransactionId();
       
        $delete = $this->json('GET', '/api/transaction/'.$transactionId.'/delete');
        $delete->assertStatus(200)
                ->assertJson([
                    'status' => 'deleted',
                ]);
    }
    
    private function getTransactionId()
    {
        $response = $this->json('GET', '/api/transactions');
        $response->assertStatus(200);
        $data = collect($response->getData())->first();
        
        return $data->transaction_id;
    }
}
