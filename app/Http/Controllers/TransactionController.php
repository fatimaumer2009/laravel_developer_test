<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Cache;


class TransactionController extends Controller
{
    /**
     * Get transaction list.
     */
    
    public function index() 
    {
        $data = $this->getData();
        
        if(!empty($data)) 
        {
            return response()->json($data, 200);
        }else
        {
            return response()->json(['message' => 'No Records Found!'], 200);
        }
    }
    
    /**
     * Save transaction.
     */
    
    public function store(Request $request)
    {
        $fields = [
            'start_date', 'end_date', 'first_name', 'last_name', 'email', 'telnumber', 'address1', 'address2', 'city', 'country', 'postcode', 'product_name', 'cost', 'currency', 'transaction_date'
        ];
        
        $validator = $this->vaildateTransaction($request);
        
        if ($validator->fails()) 
        {
            return response()->json(['status' => 'Validation Exception', 'error' => $validator->messages()], 422);
        }else
        {
            $grantData = $this->getData();
            
            $data = $request->only($fields);
            $data['transaction_id'] = rand();
            
            $grantData->push($data);
            
            $this->storeData($grantData);
            
            return response()->json(['status' => 'created', 'message' => 'Transaction Created!'], 200);
        }
    }
    
    /**
     * Update transaction.
     */
    
    public function update($id, Request $request)
    {
        $fields = [
            'start_date', 'end_date', 'first_name', 'last_name', 'email', 'telnumber', 'address1', 'address2', 'city', 'country', 'postcode', 'product_name', 'cost', 'currency', 'transaction_date',
        ];
        
        $validator = $this->vaildateTransaction($request, 1);
         
        if ($validator->fails()) 
        {
            return response()->json(['status' => 'Validation Exception', 'error' => $validator->messages()], 422);
        }else
        {
            $grantData = $this->getData();
            
            $data = $request->only($fields);
            
            $newData = $grantData->map(function($value, $key) use($id, $data)
            {
                if($value['transaction_id'] == $id)
                {
                    return array_replace($value, $data);
                }  else {
                    return $value;
                }
            });
            
            $this->storeData($newData);
                
            return response()->json(['status' => 'updated', 'message' => 'Transaction Updated!'], 200);
        }            
    }
    
    /**
     * Delete transaction.
     */
    
    public function delete($id)
    {
        $grantData = $this->getData();
        
        $key = $grantData->search(function($item) use($id)
        {
            return $item['transaction_id'] == $id;
        });
  
        if($key !== false)
        {
            $grantData->pull($key);
        
            $this->storeData($grantData);
            
            return response()->json(['status' => 'deleted', 'message' => 'Record Deleted!'], 200);
        }else
        {
            return response()->json(['message' => 'No Record Found!'], 400);
        }
    }

    /**
     * Get Product list.
     */
    
    public function productList()
    {
         return response()->json($this->products(), 200);
    }
    
    private function products()
    {
        $products = array(
            'DYSIH_NL_PLATINUM_8-14',
            'DYSIH_NL_PLATINUM_4-7',
            'DYSIH_IT_PLATINUM_1-3',
            'DYSIH_DE_PLATINUM_25-30'
        );
        
        return $products;
    }
    
    private function vaildateTransaction($request)
    {
        $data = [
            'start_date'       => 'required|date_format:d/m/Y',
            'end_date'         => 'required|date_format:d/m/Y|after:start_date',
            'first_name'       => 'required',
            'last_name'        => 'required', 
            'email'            => 'required|email',
            'telnumber'        => 'required|digits_between:8,20',
            'product_name'     => 'required|in:'.implode(',',$this->products()),
            'cost'             => 'required|numeric',
            'currency'         => 'required|in:usd,gbp',
            'transaction_date' => 'required|date_format:d/m/Y'
        ];
        
        $validator = Validator::make($request->all(), $data);
                
        return $validator;
    }
    
    private function getData()
    {
       $grantData = collect([]);
       
        if(Cache::has('transactions')) 
        {
            $grantData = Cache::get('transactions');
        }
        
        return $grantData;
    }
    
    private function storeData($grantData)
    {
        Cache::put('transactions', $grantData, 36000);
    }
}
